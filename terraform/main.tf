terraform {
  backend "http" {
  }
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.1"
    }
  }
}

variable "gitlab_access_token" {
  type = string
}

provider "gitlab" {
  token = var.gitlab_access_token
}

data "gitlab_project" "terraform_project" {
  id = "25867478"
}

resource "gitlab_project_variable" "sample_project_variable" {
  project = data.gitlab_project.terraform_project.id
  key     = "example_variable"
  value   = "Greetings World!"
}
